@title[Introduction]
## NSync
#### Monadic JavaScript in production

---?image=assets/time-2980690_1920.jpg
<span style="color:white">just 20 mins</span>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

---
## Hand Waving
<img src="https://bytebucket.org/franciscomeza/fnprog-preso/raw/620019121003675c1818ced6afe63a23d213ac62/assets/hand-2415036_1920.png" height="20%" width="20%"/>

Without upfront explanations let's just assume that there is some truth in:

- side effects, state -> higher cognitive load |
- imperative code -> non essential complexity  |
- verbose code -> more lines -> more bugs      |

---
## Related Material
#### (to cover for my hand waving)
- Out Of the Tar Pit<br/>
  <span style="font-size:0.6em;">http://curtclifton.net/papers/MoseleyMarks06a.pdf</span>
- Why Functional Programming Matters<br/>
  <span style="font-size:0.6em;">https://www.cs.kent.ac.uk/people/staff/dat/miranda/whyfp90.pdf</span>
- Professor's Frisby's Mostly Adequate Guide to Functional Programming<br/>
  <span style="font-size:0.6em;">https://drboolean.gitbooks.io/mostly-adequate-guide/</span>
- Learn You a Haskell for Great Good<br/>
  <span style="font-size:0.6em;">http://learnyouahaskell.com/</span>

---
## Problem Statement
If there's any truth in those three assertions, then we should strive for the following characteristicts in our codebases:

- as stateless and as few side effects as possible
- declarative instead of imperative
- less code

---
## Scenario 1
Typical ways to deal with null values:

---
#### NPE
![Image](assets/nuclear-weapons-test-67557_1920.jpg) <!-- .element height="80%" width="80%" -->

---
#### Control flow (non-essential complexity)
![Image](assets/telephone-1822040_1920.jpg) <!-- .element height="80%" width="80%" -->

---
```javascript
const nullable1 = couldReturnNull(someParam);

if (nullable1) {
  const nullable2 = couldReturnNullToo(nullable1);
  if (nullable2) {
    const nullable3 = meToo(nullable2);
    if (nullable3) {
      // ... and so on
    } else {
      // handle null
    }
  } else {
    // handle null
  }
} else {
  // handle null
}

// A lot of code, when all we wanted was just a vanilla function composition:
const nullable = meToo(couldReturnNullToo(couldReturnNull(someParam)));
```

@[1-17](We have to do all this...)
@[19-20](...when all we want is this)

---
## Scenario 2
Typical ways to deal with the result value of a process that may fail:

---
#### RTE
![Image](assets/nuclear-weapons-test-67557_1920.jpg) <!-- .element height="80%" width="80%" -->

---
#### Control flow (non-essential complexity)
![Image](assets/telephone-1822040_1920.jpg) <!-- .element height="80%" width="80%" -->

---
```javascript
try {
  const fallible1 = couldFail(someParam);
  try {
    const fallible2 = couldFailToo(fallible1);
    try {
      const fallible3 = meToo(fallible2);
      // ... and so on
    } catch (e) {
      // hopefully do something with e
    }
  } catch (e) {
    // hopefully do something with e
  }
} catch (e) {
  // hopefully do something with e
}

// A lot of code, when all we wanted was just a vanilla function composition:
const fallible = meToo(couldFailToo(couldFail(someParam)));
```

@[1-16](We have to do all this...)
@[18-19](...when all we want is this)

---
```javascript
couldFail(someParam, function(err, result) {
  if (err) {
    // hopefully do something with err
  } else {
    couldFailToo(result, function(err, result) {
      if (err) {
        // hopefully do something with err
      } else {
        meToo(result, function(err, result) {
          if (err) {
            // hopefully do something with err
          } else {
            // ... we made it!
          }
        });
      }
    });
  }
});

// A lot of code, when all we wanted was just a vanilla function composition:
const fallible = meToo(couldFailToo(couldFail(someParam)));
```

@[1-19](Again, all this...)
@[21-22](... for just this)

---
## Scenario 3
Typical ways to deal with validations:

---
#### DB Constraint violations
![Image](assets/nuclear-weapons-test-67557_1920.jpg) <!-- .element height="80%" width="80%" -->

---
#### Control flow (non-essential complexity)
![Image](assets/telephone-1822040_1920.jpg) <!-- .element height="80%" width="80%" -->

---
Did you notice a pattern?

- when dealing with a value/process/condition |
- if it is present/successful/satisfied       |
- we continue down the happy path             |

---
otherwise we react in a way that typically...

- ...shortcircuits the rest of the process          |
- ...pollutes a clean description of the happy path |
- ...requires boillerplate code                     |

---
What if considering all those scenarios as variations of a unique case allowed us to

- avoid side effects |
- in a declarative.. |
- ..and concise way  |

---
The good news is that they are all variations of a unique case:

Dealing with a 'value within a context'  <!-- .element: class="fragment" -->

---
## Nullable

a value whose context is whether it exists  <!-- .element: class="fragment" -->

---
## Process Result

a value whose context is whether the process that produced it succeeded  <!-- .element: class="fragment" -->

---
## Validation Result

a value whose context is whether it's consistent with a set of rules  <!-- .element: class="fragment" -->

---
so... how do we get from that realization to side-effect-free, declarative and concise code that handles 'value within a context'?

We borrow from Category Theory</br>(It being a Math field, this is where using pure functions becomes important)  <!-- .element: class="fragment" -->

- functors     |
- applicatives |
- monads       |

---
## What is a Functor?

Something that can be mapped over  <!-- .element: class="fragment" -->

Too vague? :)                      <!-- .element: class="fragment" -->

---
![Image](/assets/functormap.png)

---
![Image](/assets/fantasy-land_functor.png)  <!-- .element height="80%" width="80%" -->

<span style="font-size: 0.4em">(source: fantasyland: https://github.com/fantasyland/fantasy-land)</span>

---
## Maybe
```javascript
import Maybe from 'data.maybe';

const timesTwo = (n) => n * 2;

Maybe.Nothing().map(timesTwo) /* Maybe {} */
Maybe.Just(42).map(timesTwo)  /* Maybe { value: 84 } */
```
@[3]()
@[1,5]()
@[1,6]()

---
```javascript
const nullable1 = couldReturnNull(someParam);

if (nullable1) {
  const nullable2 = couldReturnNullToo(nullable1);
  if (nullable2) {
    const nullable3 = meToo(nullable2);
    if (nullable3) {
      // ... and so on
    } else {
      // handle null
    }
  } else {
    // handle null
  }
} else {
  // handle null
}

const result = couldReturnNull(someParam)
        .map(couldReturnNullToo)
        .map(meToo);
```
@[1-17](And now we can replace this...)
@[19-21](...with this)

---
## Either
```javascript
import Either from 'data.either';

const timesTwo = (n) => n * 2;

Either.Left('some error msg').map(timesTwo) /* Either { value: 'some error msg' } */
Either.Right(42).map(timesTwo)              /* Either { value: 84 } */
```
@[3]()
@[1,5]()
@[1,6]()

---
```javascript
try {
  const fallible1 = couldFail(someParam);
  try {
    const fallible2 = couldFailToo(fallible1);
    try {
      const fallible3 = meToo(fallible2);
      // ... and so on
    } catch (e) {
      // hopefully do something with e
    }
  } catch (e) {
    // hopefully do something with e
  }
} catch (e) {
  // hopefully do something with e
}

const result = couldFail(someParam)
        .map(couldFailToo)
        .map(meToo);
```
@[1-16](And now we can replace this...)
@[18-20](...with this)

---
## Task
Remember this?

```javascript
couldFail(someParam, function(err, result) {
  if (err) {
    // hopefully do something with err
  } else {
    couldFailToo(result, function(err, result) {
      if (err) {
        // hopefully do something with err
      } else {
        meToo(result, function(err, result) {
          if (err) {
            // hopefully do something with err
          } else {
            // ... we made it!
```

The async process case introduces the need for an additional Functor  <!-- .element: class="fragment" -->

---
**Task** represents a -not yet performed- async calculation.<br/>

By wrapping it in a **Task** we -temporarily- avoid side effects...</br>

...And because of that we can now reason about the future result as a 'value within a context' <!-- .element: class="fragment" -->

---
```javascript
import Task from 'data.task';

const timesTwo = (n) => n * 2;

Task.rejected('boom!'); // Nothing happens
Task.resolved(42);      // Nothing happens either

// Tasks need 'forking'
Task.rejected('boom!').fork(console.err, console.log); // boom!
Task.of(42).fork(console.err, console.log);            // 42

// And now we can do this
Task.rejected('boom!')
  .map(timesTwo)
  .fork((err) => console.err(`err: ${err}`), console.log); // err: boom!
Task.of(42)
  .map(timesTwo)
  .fork(console.err, (res) => console.log(`res: ${res}`)); // res: 84
```
@[1, 5,6](This is how we represent a -not yet performed- async calculation)
@[8-10](This is how we actually perform it)
@[3, 12-18](And this is how this makes our life easier)

---
## What is an Applicative? (1)

Functors allow mapping a regular function over a value within a context  <!-- .element: class="fragment" -->

What happens if we have a multi arg function?  <!-- .element: class="fragment" -->

---
## What is an Applicative? (2)

Something that allows a 'normal' function to operate on several 'values within a context'  <!-- .element: class="fragment" -->

Too vague? :)  <!-- .element: class="fragment" -->

---
![Image](/assets/naturality-diagram.png)

---
![Image](/assets/fantasy-land_applicative.png)  <!-- .element height="80%" width="80%" -->

<span style="font-size: 0.4em">(source: fantasyland: https://github.com/fantasyland/fantasy-land)</span>

---
```javascript
// how do we apply this function...
const add2 = (x, y) => x + y;

// ...to multiple 'value within a context' arguments?
const arg1 = Maybe.Just(2);
const arg2 = Maybe.Just(3);

// With an applicative (by the way, Maybe is an applicative)
Maybe.Just(R.curry(add2))  // notice the function needs to be curried
  .ap(arg1)
  .ap(arg2);               // -> Maybe { value: 5 }

// Notice how applicative respects arguments' context
Maybe.Just(R.curry(add2))
  .ap(Maybe.Nothing())
  .ap(arg2);               // -> Maybe {}

Maybe.Just(R.curry(add2))
  .ap(arg1)
  .ap(Maybe.Nothing());    // -> Maybe {}

// By the way, R.curry(add2) is equivalent to having defined add2 as x => y => x + y
// Works with Either too, but no need to be redundant I hope :)
```
@[1,2](how do we apply this function...)
@[4-6](...to multiple 'value within a context' arguments?)
@[8-11](With an applicative (by the way, Maybe is an applicative))
@[13-20](Notice how applicative respects arguments' context)
@[22]()
@[23]()

---
## What is a Monad? (1)

functors allow mapping a regular function over a 'value within a context'  <!-- .element: class="fragment" -->

applicatives allow applying a multi arg function to multiple 'values within a context'  <!-- .element: class="fragment" -->

what to do when the result of our function comes with its own context?  <!-- .element: class="fragment" -->

---
## What is a Monad? (2)

Something that allows a function that returns a 'value within a context' to operate onto a value of the same kind of context <!-- .element: class="fragment" -->

Too vague? :)  <!-- .element: class="fragment" -->

---
![Image](/assets/monad_associativity.png)

---
![Image](/assets/fantasy-land_monad.png)  <!-- .element height="80%" width="80%" -->

<span style="font-size: 0.4em">(source: fantasyland: https://github.com/fantasyland/fantasy-land)</span>

---
```javascript
const maybeTimes2 = (x) => (x > 0 ? Maybe.Just(x * 2) : Maybe.Nothing());

maybeTimes2(2);     // Maybe { value: 4 }
maybeTimes2(-2)     // Maybe {}

const aMaybe = Maybe.Just(42);

// with Functors:
aMaybe.map(maybeTimes2);    // Maybe { value: Maybe { value: 84 } }    (Eew!)

// with Monads:
aMaybe.chain(maybeTimes2);  // Maybe { value: 84 }                     (Nice!)
```
@[1-4](We have this function...)
@[6](...and this 'value within a context')
@[8,9](What happens if we 'map'...)
@[11,12](...and this is what monads are for)

---
```javascript
couldFail(someParam, function(err, result) {
  if (err) {
    // hopefully do something with err
  } else {
    couldFailToo(result, function(err, result) {
      if (err) {
        // hopefully do something with err
      } else {
        meToo(result, function(err, result) {
          if (err) {
            // hopefully do something with err
          } else {
            // ... we made it!
          }
	    });
	  }
	});
  }
}

couldFailTask(someParam)
	.chain(couldFailTooTask)
	.chain(meTooTask)
```
@[1-19](And now we can replace this...)
@[21-23](...with this)

---

What does all this look like in real life?

---
```
// some 'imports' here

function syncCommercialTest(sfdcConn, defaults, commercialTest) {
  const _genes = cm.getTestGenes(commercialTest.code);
  return _genes.chain(function(genes) {
    return genes.cata({
      Left: R.compose(Task.of, util.reportError),
      Right: R.curry(sf.syncCommercialTest)(sfdcConn, defaults, commercialTest)
    });
  });
}

function syncConceptMapperVersion(sfdcConn, defaults, version) {
  return cm.getCommercialTests(version)
    .chain(function(ts) {
      const syncTasks = ts.map(R.curry(syncCommercialTest)(sfdcConn, defaults));
      return monads.sequence(Task, syncTasks);
    }).chain(function(results) {
      return sf.confirmMapperVersionSyncd(sfdcConn, version)
        .map((_) => results);
    }).map(function(results) {
      return [version, Maybe.Just(results)];
    });
}

function syncConceptMapperVersions(log, sfdcConn, defaults, versions) {
  log.info(`Mapper versions to sync: ${versions}`);

  return monads.sequence(Task, versions.map(R.curry(syncConceptMapperVersion)(sfdcConn, defaults)))
    .map(R.fromPairs);
}

function sync(log, sfdcConn) {
  const _syncTask = Task.of(R.curry(
    (defaults, boVersions, sfdcVersions) => syncConceptMapperVersions(log,
                                                                      sfdcConn,
                                                                      defaults,
                                                                      R.difference(boVersions, sfdcVersions))
  ));

  return monads.join(_syncTask
                     .ap(sf.getDefaultsForCommercialTestSyncing(sfdcConn))
                     .ap(cm.getVersions())
                     .ap(sf.getConceptMapperVersions(sfdcConn)));
}

export default function main(log) {
  return sf.login()
    .chain((sfdcConn) => sync(log, sfdcConn))
    .map(summary);
}
```
@[47-51](Entry point; only exported function)
@[33-45](To sync everything...)
@[34-38](...apply this applicative...)
@[42-44](...to these arguments)
@[26-31](The applicative consists of sequentially syncing all versions in BO and not in SFDC)
@[13-24](For each version...)
@[14](...find all commercial test -concepts-...)
@[15-18](...sync them one at a time...)
@[18-21](...mark the version as synced...)
@[21-23](...return results for reporting)
@[3-11](And finally, the syncing of a single commercial test consists of...)
@[4-5](...finding all test genes and...)
@[6,7](...either report an API error)
@[6,8](...or sync all test genes to SFDC...)

---
This is what getting a connection to SFDC looks like

```
export function login() {
  return new Task(function(reject, resolve) {
    let sfdcConn = new jsforce.Connection({ loginUrl: config.salesforce.loginurl });
    sfdcConn.login(config.salesforce.userid,
                   config.salesforce.password,
                   function(err, userInfo) {
                     if (err) {
                       return reject(`Error connecting to SFDC:\n${err}`);
                     }
                     return resolve(sfdcConn);
                   });
  });
}
```
@[3,4,8,10](We wrap invocations to the underlying jsforce library in a **Task**)

---
This is the code to sync the genes of a single commercial test

```
export function syncCommercialTest(sfdcConn, {standardPriceBook}, commercialTest, genes) {
  const _mapper = sfmapper.buildProductPayload;
  return sfutil.safeActionTask(sfdcConn, OBJECT_PRODUCT, ACTION_INSERT, _mapper(commercialTest, genes))
    .chain(function(res) {
      return sfutil.safeActionTask(
        sfdcConn, OBJECT_PRICEBOOK_ENTRY, ACTION_INSERT,
        {
          Pricebook2Id: standardPriceBook.Id,
          Product2Id: res.id,
          UnitPrice: 0
        }
      ).map((_) => res);
    });
}
```
@[3](We also wrap everything in a **Task**, but additionally...)
@[2](...notice the use of a _mapper_)

---
(Strictly speaking, not really monadic JavaScript, but still worth showing it as an example of declarative programming)

---
```javascript
const PRODUCT_INSERT = { // mapping: f(commercialTest, genes)
  Clinical_Area__c: R.compose(R.join(', '),
                              R.map(R.propOr('', 'name')),
                              R.filter(R.propEq('concept_type', 'main_category')),
                              R.propOr([], 'categories')),
  Family: R.always('Commercial Tests'),
  Genes_Included__c: (_1, genes) => R.join(', ', genes),
  Mapper_Version__c: R.prop('mapper_version'),
  Name: R.prop('name'),
  NY_Approved__c: R.compose(R.contains('NY Approved'),
                            R.propOr([], 'tags')),
  Order_Group__c: R.compose(R.join(', '),
                            R.map(R.propOr('', 'name')),
                            R.filter(R.propEq('concept_type', 'order_group')),
                            R.propOr([], 'categories')),
  ProductCode: R.prop('code'),
  Subcategory__c: R.compose(R.join(', '),
                            R.map(R.propOr('', 'name')),
                            R.filter(R.propEq('concept_type', 'sub_category')),
                            R.propOr([], 'categories'))
};

export function buildProductPayload(commercialTest, genes) {
  return buildPayload(null, { mappingsForInsert: PRODUCT_INSERT }, [commercialTest, genes]);
}
```
@[23-25](Here we specify the _mappings_ to use and the argument for them)
@[1-21](And this is the declarative description of the mapping)
@[2-5](E.g. the clinical area is defined via composition as...)
@[2](...a comma separated string...)
@[3](...of the names...)
@[5](...of the commercial test _categories_...)
@[4](...whose _concept<span>&#95;</span>type_ is main_category...)

---

<span style="font-size: 2em">Questions?</span>
